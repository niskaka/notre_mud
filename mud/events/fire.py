# -*- coding: utf-8 -*-

from .event import Event2

class FireEvent(Event2):
    NAME = "fire"

    def perform(self):
        if not self.object.has_prop("firable"):
            self.add_prop("object-not-firable")
            return self.take_failed()
        if self.object in self.actor:
            self.add_prop("object-already-in-inventory")
            return self.take_failed()
        self.object.move_to(self.actor)
        self.inform("fire")

    def take_failed(self):
        self.fail()
        self.inform("fire.failed")