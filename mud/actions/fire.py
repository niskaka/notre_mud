# -*- coding: utf-8 -*-

from .action import Action2
from mud.events import


class FireAction(Action2):
    EVENT = FireEvent    
    RESOLVE_OBJECT = "resolve_for_fire"
    ACTION = "fire"